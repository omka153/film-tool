# Film Tool for Nulight Studios
This is an Android app to help calculate and identify certain aspects of any given film reel. 

This contains **three features**.

#### 1. Motion picture film calculator

This accepts **user input** to work out the following:  

*  Film reel length (ft) using film core and reel diameter (mm)  

*  Film reel length (ft/m) using film duration (hh:mm:ss), film format and framerate

*  Film reel duration (hh:mm:ss) using film reel length (ft/m), film format and framerate  

*  File size (GB) using file format, framerate and runtime (hh:mm:ss)

*  Runtime (hh:mm:ss) using file format, framerate and file size (GB)


#### 2. Film format identification guide

This is a **flowchart** with graphical aids, that **asks simple yes/no questions** to the user in order to identify the format of the film reel.

#### 3. Automated film calculator

This uses the **camera** to measure the inner and outer diameter of a film reel.  

Once this is done, an estimate of the film length, runtime and filesize will be calculated and shown.

(Currently incomplete!)


## Other considerations
The constants used in calculation, or framerate/file format/film format can be **easily modified** by a non-technical admin for the interests of futureproofing.  

The app was made for Nulight Studios, and thus carries their branding.





